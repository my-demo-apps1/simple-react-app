## simple-react-app

simple-react-app is a demo Single Page Application which uses:
* ReactJs
* Functional Components
* Hooks (useState, useEffect)
* Redux
* Thunks
* Jest for Unit Tests
* Semantic UI Library
* React-Router
* Responsive Layout
* Integration with AWS API Gateway (used Lambda + DynamoDB)



## Integration with Amazon Web Services
Integrates with REST API build on AWS APIGateway + LambdaFunctions + DynamoDB

