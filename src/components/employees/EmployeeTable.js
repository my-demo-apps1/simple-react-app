import React, { useState } from "react";
import {
  Grid,
  Icon,
  Pagination,
  Popup,
  Table,
  TransitionablePortal
} from "semantic-ui-react";
import { isNil, sortBy, startCase } from "lodash";
import { Link } from "react-router-dom";
import MessageBox from "../extension/MessageBox";

const handlePageChange = () => {
  return (
    <TransitionablePortal open={true}>
      <p>Not yet implmented</p>
    </TransitionablePortal>
  );
};

const ActionCell = ({ keyValue }) => {
  const editActionLink = `/dynamictable/employee/edit/${keyValue}`;
  const deleteActionLink = `/dynamictable/employee/delete/${keyValue}`;
  return (
    <Table.Cell>
      <Link to={editActionLink} title="Edit Employee">
        <Icon name="edit" />
      </Link>

      <Link to={deleteActionLink} title="Delete Employee">
        <Icon name="delete" />
      </Link>
    </Table.Cell>
  );
};

const EmployeeTable = ({
  employeeData,
  showActionColumn,
  keyColumn,
  keyValueToHighLight
}) => {
  const [tableConfig, setTableConfig] = useState({
    sortDirection: null,
    sortColumn: null,
    data: employeeData
  });

  if (isNil(employeeData)) {
    return <MessageBox title="Table Demo" message="No results to display" />;
  }

  const handleSort = (clickedColumn, curtableConfig) => () => {
    const { sortColumn, data, sortDirection } = curtableConfig;
    if (sortColumn !== clickedColumn) {
      setTableConfig({
        sortColumn: clickedColumn,
        data: sortBy(data, [clickedColumn]),
        sortDirection: "ascending"
      });
      return;
    }
    setTableConfig({
      sortColumn: clickedColumn,
      data: data.reverse(),
      sortDirection: sortDirection === "ascending" ? "descending" : "ascending"
    });
  };

  const { data } = tableConfig;

  const dataKeys = Object.keys(data[0]);
  let actionColumn = null;
  if (!isNil(showActionColumn) && showActionColumn) {
    actionColumn = <Table.HeaderCell key="actions">Actions</Table.HeaderCell>;
  }

  return (
    <Grid>
      <Grid.Column width={16}>
        <Table color="teal" singleLine striped selectable>
          <Table.Header>
            <Table.Row>
              {dataKeys.map(key => {
                return (
                  <Table.HeaderCell
                    sorted={
                      tableConfig.sortColumn === key
                        ? tableConfig.sortDirection
                        : null
                    }
                    onClick={handleSort(key, tableConfig)}
                    key={`header-cell-${key}`}
                  >
                    {tableConfig.sortColumn === key ? (
                      tableConfig.sortDirection === "ascending" ? (
                        <Icon name="caret down" />
                      ) : (
                        <Icon name="caret up" />
                      )
                    ) : null}
                    {startCase(key)}
                  </Table.HeaderCell>
                );
              })}
              {actionColumn}
            </Table.Row>
          </Table.Header>

          <Table.Body>
            {tableConfig.data.map((item, index) => {
              const highlightRow =
                !isNil(keyValueToHighLight) &&
                item[keyColumn] === keyValueToHighLight;
              return (
                <Table.Row key={`row-${index}`} positive={highlightRow}>
                  {dataKeys.map(key => (
                    <Table.Cell key={`cell-${index}.${key}`}>
                      {item[key]}
                    </Table.Cell>
                  ))}
                  {!isNil(showActionColumn) && showActionColumn && (
                    <ActionCell keyValue={item[keyColumn]} />
                  )}
                </Table.Row>
              );
            })}
          </Table.Body>
          <Table.Footer></Table.Footer>
        </Table>
        {/* <Pagination
          fluid
          defaultActivePage={5}
          totalPages={10}
          onClick={handlePageChange}
        /> */}
      </Grid.Column>
    </Grid>
  );
};

export default EmployeeTable;
