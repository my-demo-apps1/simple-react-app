import React from "react";
import { Label } from "semantic-ui-react";

const EmployeeDetail = ({
  Firstname,
  Lastname,
  Title,
  Department,
  EmployeeId
}) => {
  return (
    <div>
      <Label size="large">
        Firstname:
        <Label.Detail>{Firstname}</Label.Detail>
      </Label>
      <Label size="large">
        Lastname:
        <Label.Detail>{Lastname}</Label.Detail>
      </Label>

      <Label size="large">
        Title:
        <Label.Detail>{Title}</Label.Detail>
      </Label>

      <Label size="large">
        Department:
        <Label.Detail>{Department}</Label.Detail>
      </Label>
    </div>
  );
};

export default EmployeeDetail;
