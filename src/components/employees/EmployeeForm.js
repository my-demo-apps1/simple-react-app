import React from "react";
import { withFormik } from "formik";
import { Button, Form, Menu } from "semantic-ui-react";
import { Link } from "react-router-dom";
import { Input, Dropdown } from "../controls/index";

const RenderForm = ({ handleSubmit, handleReset, isSubmitting }) => (
  <Form onSubmit={handleSubmit}>
    <Form.Group widths="2">
      <Input label="First Name" name="firstname"/>
      <Input label="Last Name" name="lastname" />
    </Form.Group>

    <Form.Group widths="2">
      <Dropdown
        label="Title"
        name="title"
        options={[
          { text: "--Choose One--", value: "empty" },
          { text: "HR Manager", value: "HR Manager" },
          { text: "Payroll Admin", value: "Payroll Admin" },
          { text: "Sales Executive", value: "Sales Executive" },
          { text: "Sr. Sofware Developer", value: "Sr. Sofware Developer" },
          { text: "UX Designer II", value: "UX Designer II" },
          { text: "UX Designer", value: "UX Designer" }
        ]}
      />
      <Dropdown 
        label="Department"
        name="department"
        options={[
          { text: "--Choose One--", value: "empty" },
          { text: "Information Technology", value: "Information Technology" },
          { text: "Human Resources", value: "Human Resources" },
          { text: "Marketing", value: "Marketing" },
          { text: "Payroll", value: "Payroll" }
        ]}
      />
    </Form.Group>

    <Button type="submit" loading={isSubmitting} primary>
      Submit
    </Button>
    <Menu.Item as={Link} to={"/dynamictable"} name="back">
      <Button type="button" basic>
        Cancel
      </Button>
    </Menu.Item>
  </Form>
);

const EmployeeForm = withFormik({
  mapPropsToValues: ({
    Firstname,
    Lastname,
    Title,
    Department,
    EmployeeId
  }) => ({
    firstname: Firstname,
    lastname: Lastname,
    title: Title,
    department: Department,
    id: EmployeeId
  }),

  // Custom sync validation
  validate: values => { 
    const errors = {};

    if (!values.firstname) {
      errors.firstname = "* Required";
    }

    if (!values.lastname) {
      errors.lastname = "* Required";
    }

    if (!values.title || values.title==='empty') {
      errors.title = "* Required";
    }

    if (!values.department || values.department==='empty') {
      errors.department = "* Required";
    }

    return errors;
  },

  handleSubmit: (values, formikApi) => {
    const {
      setSubmitting,
      props: { handleSave }
    } = formikApi; 
    handleSave(
      values,
      () => {
        setSubmitting(false);
      }
    );
  },

  displayName: "Edit Employee:"
})(RenderForm);

export default EmployeeForm;
