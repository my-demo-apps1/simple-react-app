﻿import React from "react";
import { Route } from "react-router";
import Layout from "./Layout";
import Home from "../pages/Home";
import FormsDemo from "../pages/FormsDemo";
import StaticTable from "../pages/StaticTable";
import DynamicTable from "../pages/employees/EmployeeList";
import EmployeeEdit from "../pages/employees/EmployeeEdit";
import EmployeeAdd from "../pages/employees/EmployeeAdd";
import EmployeeDelete from "../pages/employees/EmployeeDelete";
import BinaryTree from "../pages/BinaryTree";
import ArrayAlgorithms from "../pages/ArrayAlgorithms";


export default (props) => (
  <Layout>
    <Route exact path="/statictable" component={StaticTable} />
    <Route exact path="/dynamictable/employee/edit/:id" component={EmployeeEdit} />
    <Route exact path="/dynamictable/employee/add/" component={EmployeeAdd} />
    <Route exact path="/dynamictable/employee/delete/:id" component={EmployeeDelete} />
    <Route exact path="/dynamictable/:id?" component={DynamicTable} />
    <Route exact path="/binarytree" component={BinaryTree} />
    <Route exact path="/forms" component={FormsDemo} />
    <Route exact path="/arrays" component={ArrayAlgorithms} />
    <Route exact path="/" component={Home} />
  </Layout>
);
