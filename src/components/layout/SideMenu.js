import React, { Component } from "react";
import { Label, Menu } from "semantic-ui-react";
import { Link } from "react-router-dom";
import TextIcon from "../extension/TextIcon";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { actionCreators } from "../../reducers/SideMenu";

class SideMenu extends Component {
  state = {
    activeItem: "dashboard"
  };

  handleItemClick = (e, { name }) => this.setState({ activeItem: name });
  changeSize = () => this.setState({ smallSidebar: !this.props.smallMenu });

  getMenu() {
    const { activeItem } = this.state;
    return (
      <Menu
        fixed="left"
        borderless
        className={(this.props.smallMenu ? "small-side" : "") + " side"}
        vertical
      >
        <Menu.Item
          as={Link}
          to={"/"}
          name="home"
          active={activeItem === "home"}
          onClick={this.handleItemClick}
        >
          <TextIcon hideText={this.props.smallMenu} color="teal" name="home">
            Home
          </TextIcon>
        </Menu.Item>

        <Menu.Item
          as={Link}
          to={"/dynamictable"}
          name="dynamictable"
          active={activeItem === "dynamictable"}
          onClick={this.handleItemClick}
        >
          <TextIcon hideText={this.props.smallMenu} color="teal" name="th list">
            Dynamic Table
          </TextIcon>
        </Menu.Item>

        {/* <Menu.Item
          as={Link}
          to={"/statictable"}
          name="statictable"
          active={activeItem === "statictable"}
          onClick={this.handleItemClick}
        >
          <TextIcon hideText={this.props.smallMenu} color="teal" name="th list">
            Static Table
          </TextIcon>
        </Menu.Item> */}

        <Menu.Item
          as={Link}
          to={"/forms"}
          name="forms"
          active={activeItem === "forms"}
          onClick={this.handleItemClick}
        >
          <TextIcon hideText={this.props.smallMenu} color="teal" name="edit">
            Forms
          </TextIcon>
        </Menu.Item>

        <Menu.Item
          as={Link}
          to={"/binarytree"}
          name="BinaryTree"
          active={activeItem === "binarytree"}
          onClick={this.handleItemClick}
        >
          <TextIcon hideText={this.props.smallMenu} color="teal" name="boxes">
            Binary Tree Algorithms
          </TextIcon>
        </Menu.Item>

        <Menu.Item
          as={Link}
          to={"/arrays"}
          name="arrays"
          active={activeItem === "arrays"}
          onClick={this.handleItemClick}
        >
          <TextIcon
            hideText={this.props.smallMenu}
            color="teal"
            name="chess board"
          >
            Array Algorithms
          </TextIcon>
        </Menu.Item>
      </Menu>
    );
  }

  render() {
    return (
      <div className="parent">
        <div className={(this.props.smallMenu ? "small-side " : "") + "side"}>
          {this.getMenu()}
        </div>
        <div
          className={(this.props.smallMenu ? "small-content " : "") + "content"}
        >
          {this.props.children}
        </div>
      </div>
    );
  }
}

export default connect(
  state => state.sideMenu,
  dispatch => bindActionCreators(actionCreators, dispatch)
)(SideMenu);
