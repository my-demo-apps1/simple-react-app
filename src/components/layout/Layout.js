import React from "react";
import { useLocation } from "react-router-dom";
import {
  Image,
  Header,
  Container
} from "semantic-ui-react";
// import SideMenu from "../layout/SideMenu";
import TopMenu from "../layout/TopMenu";

export default props => {
  const location = useLocation();
  return (
  <Container className="my-container">
    <Header as="h2" icon textAlign="center">
    <Image size='large' src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/64px-React-icon.svg.png" />
    Vikram's ReactJs Demo
      <Header.Subheader>
        This page contains some live working samples that are implemented using ReactJs, .NET, Node.Js and AWS.
      </Header.Subheader>
    </Header>
    <div className="grid">
      <div className="menu">
        <TopMenu location={location} />
      </div>
      <div className="main-content">
        {/* <SideMenu> */}
        {props.children}
        {/* </SideMenu> */}
      </div>
    </div>
  </Container>
);
  }
