import React, { Component } from "react";
import { Menu } from "semantic-ui-react";
import { Link } from "react-router-dom";
import TextIcon from "../extension/TextIcon";
import { actionCreators as sideAction } from "../../reducers/SideMenu";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";

class TopMenu extends Component {
  state = {
    activeItem: null
  };

  handleItemClick = (e, { name }) => this.setState({ activeItem: name });

  render() {
    let {activeItem} = this.state;
    const { location : { pathname } } = this.props;
    if(!activeItem) activeItem = pathname;
    console.log('activeITem->', activeItem );
    return (
      <>
      <Menu pointing secondary  className="top-menu">
      <Menu.Item
          as={Link}
          to={"/"}
          name="/home"
          active={activeItem === "/home" || activeItem === "/"}
          onClick={this.handleItemClick}
        >
          <TextIcon hideText={this.props.smallMenu}  name="home">
            Home
          </TextIcon>
        </Menu.Item>

        <Menu.Item
          as={Link}
          to={"/dynamictable"}
          name="/dynamictable"
          active={activeItem === "/dynamictable"}
          onClick={this.handleItemClick}
        >
          <TextIcon hideText={this.props.smallMenu}  name="th list">
            Dynamic Table
          </TextIcon>
        </Menu.Item>

        {/* <Menu.Item
          as={Link}
          to={"/statictable"}
          name="statictable"
          active={activeItem === "statictable"}
          onClick={this.handleItemClick}
        >
          <TextIcon hideText={this.props.smallMenu} color="teal" name="th list">
            Static Table
          </TextIcon>
        </Menu.Item> */}

        <Menu.Item
          as={Link}
          to={"/forms"}
          name="/forms"
          active={activeItem === "/forms"}
          onClick={this.handleItemClick}
        >
          <TextIcon hideText={this.props.smallMenu}  name="edit">
            Forms
          </TextIcon>
        </Menu.Item>

        <Menu.Item
          as={Link}
          to={"/binarytree"}
          name="/binarytree"
          active={activeItem === "/binarytree"}
          onClick={this.handleItemClick}
        >
          <TextIcon hideText={this.props.smallMenu}  name="boxes">
            Binary Tree Algorithms
          </TextIcon>
        </Menu.Item>

        <Menu.Item
          as={Link}
          to={"/arrays"}
          name="/arrays"
          active={activeItem === "/arrays"}
          onClick={this.handleItemClick}
        >
          <TextIcon
            hideText={this.props.smallMenu}
            name="chess board"
          >
            Array Algorithms
          </TextIcon>
        </Menu.Item>
    </Menu>
  </>
    );
  }
}

export default connect(
  state => state.sideMenu,
  dispatch => {
    return {
      actions: bindActionCreators(Object.assign({}, sideAction), dispatch)
    };
  }
)(TopMenu);
