import React from "react";
import EmployeeTable from "../employees/EmployeeTable";
import MessageBox from "../extension/MessageBox";

const employeeData = {
  Items: [
    {
      EmployeeId: "A123456",
      Title: "Sr. Sofware Developer",
      Firstname: "John",
      Lastname: "Doe",
      Department: "Information Technology"
    },
    {
      EmployeeId: "H012345",
      Title: "UX Designer",
      Firstname: "Mannie",
      Lastname: "Moe",
      Department: "Information Technology"
    },
    {
      EmployeeId: "M012345",
      Title: "HR Manager",
      Firstname: "Harry",
      Lastname: "Vine",
      Department: "Human Resources"
    },
    {
      EmployeeId: "K123456",
      Title: "Payroll Admin",
      Firstname: "John",
      Lastname: "Wick",
      Department: "Payroll"
    },
    {
      EmployeeId: "ZA34678",
      Title: "Sales Executive",
      Firstname: "Billy",
      Lastname: "Mays",
      Department: "Marketing"
    },
    {
      EmployeeId: "D012345",
      Title: "Sr. Sofware Developer",
      Firstname: "Jane",
      Lastname: "Doe",
      Department: "Information Technology"
    }
  ],
  Count: 6,
  ScannedCount: 6
};

const Info = () => {
  const message = (
    <ul>
      <li>Semantic UI Table</li>
      <li>Hardcoded Json</li>
    </ul>
  );
  return <MessageBox title="Key Components in this Demo" message={message} />;
};

const GridDemo = () => {
  return (
    <>
      <Info />
      <EmployeeTable employeeData={employeeData.Items} />
    </>
  );
};

export default GridDemo;
