import React, { useState, useEffect } from "react";
import { Header, Grid } from "semantic-ui-react";
import BinaryTree from "../binaryTrees/BinaryTree";
import NumbersArrayInput from "../forms/NumbersArrayInput";

// Hook
function useWindowSize() {
  const isClient = typeof window === "object";

  function getSize() {
    return {
      width: isClient ? window.innerWidth : undefined,
      height: isClient ? window.innerHeight : undefined
    };
  }

  const [windowSize, setWindowSize] = useState(getSize);

  useEffect(() => {
    if (!isClient) {
      return false;
    }

    function handleResize() {
      setWindowSize(getSize());
    }

    window.addEventListener("resize", handleResize);
    return () => window.removeEventListener("resize", handleResize);
  }, []);

  return windowSize;
}
// const Info = () => {
//   const message = (
//     <ul>
//       <li>Brainstorming ideas. Coming soon...</li>
//     </ul>
//   );
//   return <MessageBox title="Key Components in this Demo" message={message} />;
// };

export default props => {
  const [numbers, setNumbers] = useState(
    "8,10,12,5,3,6,1,20,32,7,11,67,89,25,35,36,57"
  );
  const [treeType, setTreeType] = useState("bt");
  const size = useWindowSize();

  const handleUpdate = (nums, treeType) => {
    setNumbers(nums);
    setTreeType(treeType);
  };

  const numbersArray = numbers.split(",");

  if (treeType === "bbt") {
    numbersArray.sort(function(a, b) {
      return a - b;
    });
  }

  return (
    <>
      <Header as="h3">Demo of Binary Tree Algorithms using Javascript</Header>

      <Grid>
        <Grid.Row columns={2}>
          <Grid.Column computer={5} tablet={15} mobile={15}>
            <NumbersArrayInput handleUpdate={handleUpdate} numbers={numbers} />
          </Grid.Column>
          <Grid.Column computer={10} tablet={15} mobile={15}>
            <BinaryTree
              numbers={numbersArray}
              width={size.width * 0.95 * (9 / 16)}
              height={size.height * 0.5}
              treeType={treeType}
              balanceTree={treeType === "bbt"}
            />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </>
  );
};
