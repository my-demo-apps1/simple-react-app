import React from "react";
import Example from "../forms/Example";
import Registration from "../forms/Registration";
import { Grid, Card, Divider, Header } from "semantic-ui-react";
import MessageBox from "../extension/MessageBox";

const Info = () => {
  const message = (
    <ul>
      <li>Semantic UI Form</li>
      <li>Formik and Yup for Validations</li>
      <li>Responsive Layout</li>
    </ul>
  );
  return <MessageBox title="Key Components in this Demo" message={message} />;
};

const FormsDemo = props => (
  <>
    <Info />
    <Grid>
      <Grid.Column mobile={16} tablet={15} computer={8}>
        <Card fluid>
          <Card.Content extra>
            <Header as="h3" textAlign="left">
              Registration Form Demo
            </Header>
            <Divider hidden />
            <Registration />
          </Card.Content>
        </Card>
      </Grid.Column>
      <Grid.Column mobile={16} tablet={15} computer={8}>
        <Card fluid>
          <Card.Content extra>
            <Header as="h3" textAlign="left">
              Form Fields Demo
            </Header>
            <Divider hidden />
            <Example />
          </Card.Content>
        </Card>
      </Grid.Column>
    </Grid>
  </>
);

export default FormsDemo;
