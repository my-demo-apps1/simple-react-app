import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { Link, useParams } from "react-router-dom";
import {
  Button,
  Loader,
  Menu,
  Card,
  Divider,
  Header
} from "semantic-ui-react";
import { isNil } from "lodash";
import getEmployee from "../../../actions/employees/Get";
import deleteEmployee from "../../../actions/employees/Delete";
import resetAllEmployees from "../../../actions/employees/ResetAll";
import MessageBox from "../../extension/MessageBox";
import EmployeeDetail from "../../employees/EmployeeDetail";

const Info = () => {
  const message = (
    <ul>
      <li>Invokes GET Method on AWS REST API to fetch Employee Record</li>
      <li>Invokes DELETE Method on AWS REST API to update Employee Record</li>
      <li>Uses React-Redux</li>
      <li>Uses Formik + Yup Components</li>
    </ul>
  );
  return <MessageBox title="Key Components in this Demo" message={message} />;
};

const DeleteSuccess = () => {
  const backButton = (
    <p>
      {" "}
      <Menu.Item as={Link} to={"/dynamictable/"} name="back">
        <Button type="button" positive>
          Ok, Show List
        </Button>
      </Menu.Item>
    </p>
  );
  return (
    <>
      <MessageBox title="Successfully deleted employee data." color="green" />
      {backButton}
    </>
  );
};

const DeleteError = () => {
  return (
    <MessageBox
      title="An error occured while processing your request."
      color="red"
    />
  );
};

const FORM_STATUS_ENTRY = "ENTRY";
const FORM_STATUS_SAVED = "SAVED";
const FORM_STATUS_ERROR = "ERROR";
// const FORM_STATUS_REVIEW = "REVIEW";

const getData = ({ id, fetchData, selectedEmployee }) => {
  return fetchData(id);
};

const Actions = ({
  id,
  handleDelete,
  isSubmitting,
  showBackButtonOnly = false
}) => {
  const isDeletable = id && id.length > 10;
  const showDeleteButton = !showBackButtonOnly && isDeletable;
  return (
    <>
      <Divider hidden />
      {showDeleteButton && (
        <Header as="h5" textAlign="left">
          Are you sure that you want to delete this employee?
        </Header>
      )}

      {!isDeletable && (
        <Header as="h5" textAlign="left">
          This record is needed for demo purposes and hence cannot be Deleted. Please select a employee record where
          length of Employee ID is > 10.
        </Header>
      )}
      {showDeleteButton && (
        <Button
          type="submit"
          loading={isSubmitting}
          negative
          onClick={() => handleDelete(id)}
        >
          Delete
        </Button>
      )}
      <Menu.Item as={Link} to={"/dynamictable"} name="back">
        <Button type="button" basic>
          Cancel
        </Button>
      </Menu.Item>
    </>
  );
};
const EmployeeDeleteCore = props => {
  const [formStatus, setFormStatus] = useState(FORM_STATUS_ENTRY);
  const [isSubmitting, setIsSubmitting] = useState(false);
  useEffect(() => getData(props), []);
  const { id } = props;
  const saveData = id => {
    setIsSubmitting(true);
    deleteEmployee(
      id,
      response => {
        props.clearEmployeesList();
        setFormStatus(FORM_STATUS_SAVED);
        setIsSubmitting(false);
      },
      () => {
        setFormStatus(FORM_STATUS_ERROR);
        setIsSubmitting(false);
      }
    );
  };

  const { selectedEmployee, hasErrored, isPending } = props; 

  if (hasErrored) {
    return (
      <>
        <DeleteError /> <Actions showBackButtonOnly={true} />
      </>
    );
  } else if (isPending) {
    return (
      <>
        <div>Fetching data...</div>
        <Loader active inline="centered" />
      </>
    );
  } else {
    if (isNil(selectedEmployee) || !selectedEmployee) {
      return <div>Data is Empty</div>;
    }

    return (
      <div>
        {formStatus === FORM_STATUS_ERROR && <DeleteError />}
        {formStatus === FORM_STATUS_SAVED && <DeleteSuccess id={id} />}
        {(formStatus === FORM_STATUS_ENTRY ||
          formStatus === FORM_STATUS_ERROR) && (
          <>
            <EmployeeDetail {...selectedEmployee} />
            <Actions
              id={id}
              handleDelete={saveData}
              isSubmitting={isSubmitting}
            />
          </>
        )}
      </div>
    );
  }
};

const EmployeeDelete = props => {
  let { id } = useParams();
  return (
    <>
      <Info />
      <Card fluid>
        <Card.Content extra>
          <Header as="h3" textAlign="left">
            Delete Employee:
          </Header>
          <Divider hidden />
          <EmployeeDeleteCore id={id} {...props} />
        </Card.Content>
      </Card>
    </>
  );
};

const mapStateToProps = state => {
  return {
    selectedEmployee: state.employees.selectedEmployee,
    hasErrored: state.employees.hasErrored,
    isPending: state.employees.isPending
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchData: id => dispatch(getEmployee(id)),
    clearEmployeesList: () => dispatch(resetAllEmployees())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EmployeeDelete);
