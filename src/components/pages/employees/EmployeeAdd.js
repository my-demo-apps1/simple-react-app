import React, { useState } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { Button, Menu, Card, Divider, Header } from "semantic-ui-react"; 
import addEmployee from "../../../actions/employees/Add";
import resetAllEmployees from "../../../actions/employees/ResetAll";
import MessageBox from "../../extension/MessageBox";
import EmployeeForm from "../../employees/EmployeeForm";

const Info = () => {
  const message = (
    <ul>
      <li>Invokes GET MEthod on AWS REST API to fetch Employee Record</li>
      <li>Invokes POST MEthod on AWS REST API to update Employee Record</li>
      <li>Uses React-Redux</li>
      <li>Uses Formik + Yup Components</li>
    </ul>
  );
  return <MessageBox title="Key Components in this Demo" message={message} />;
};

const SaveSuccess = () => {
  const backButton = (
    <p>
      {" "}
      <Menu.Item as={Link} to={"/dynamictable/"} name="back">
        <Button type="button" positive>
          Ok, Show List
        </Button>
      </Menu.Item>
    </p>
  );
  return (
    <>
      <MessageBox title="Successfully added employee data." color="green" />
      {backButton}
    </>
  );
};

const SaveError = () => {
  return (
    <MessageBox
      title="An error occured while processing your request."
      color="red"
    />
  );
};

const FORM_STATUS_ENTRY = "ENTRY";
const FORM_STATUS_SAVED = "SAVED";
const FORM_STATUS_ERROR = "ERROR";
// const FORM_STATUS_REVIEW = "REVIEW";

const EmployeeAddCore = props => {
  const [formStatus, setFormStatus] = useState(FORM_STATUS_ENTRY);

  const saveData = (values, handleError) => {
    addEmployee(
      values,
      response => {
        props.clearEmployeesList();
        setFormStatus(FORM_STATUS_SAVED);
      },
      () => {
        setFormStatus(FORM_STATUS_ERROR);
        handleError();
      }
    );
  };

  return (
    <div>
      {formStatus === FORM_STATUS_ERROR && <SaveError />}
      {formStatus === FORM_STATUS_SAVED && <SaveSuccess />}
      {(formStatus === FORM_STATUS_ENTRY ||
        formStatus === FORM_STATUS_ERROR) && (
        <EmployeeForm handleSave={saveData} />
      )}
    </div>
  );
};

const EmployeeAdd = props => {
  return (
    <>
      <Info />
      <Card fluid>
        <Card.Content extra>
          <Header as="h3" textAlign="left">
            Add Employee:
          </Header>
          <Divider hidden />
          <EmployeeAddCore {...props} />
        </Card.Content>
      </Card>
    </>
  );
};

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    clearEmployeesList: () => dispatch(resetAllEmployees())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EmployeeAdd);
