import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { Link, useParams } from "react-router-dom";
import {
  Button,
  Loader,
  Menu, 
  Card,
  Divider,
  Header
} from "semantic-ui-react";
import { isNil } from "lodash";
import getEmployee from "../../../actions/employees/Get";
import updateEmployee from "../../../actions/employees/Update";
import resetAllEmployees from "../../../actions/employees/ResetAll";
import MessageBox from "../../extension/MessageBox";
import EmployeeForm from "../../employees/EmployeeForm";

const Info = () => {
  const message = (
    <ul>
      <li>Invokes GET MEthod on AWS REST API to fetch Employee Record</li>
      <li>Invokes POST MEthod on AWS REST API to update Employee Record</li>
      <li>Uses React-Redux</li>
      <li>Uses Formik + Yup Components</li>
    </ul>
  );
  return <MessageBox title="Key Components in this Demo" message={message} />;
};

const SaveSuccess = ({ id }) => {
  const backButton = (
    <p>
      {" "}
      <Menu.Item as={Link} to={"/dynamictable/" + id} name="back">
        <Button type="button" positive>
          Ok, Show List
        </Button>
      </Menu.Item>
    </p>
  );
  return (
    <>
      <MessageBox title="Successfully updated employee data." color="green" />
      {backButton}
    </>
  );
};

const SaveError = () => {
  return (
    <MessageBox
      title="An error occured while processing your request."
      color="red"
    />
  );
};

const FORM_STATUS_ENTRY = "ENTRY";
const FORM_STATUS_SAVED = "SAVED";
const FORM_STATUS_ERROR = "ERROR";
// const FORM_STATUS_REVIEW = "REVIEW";

const getData = ({ id, fetchData, selectedEmployee }) => {
  return fetchData(id);
};

const EmployeeEditCore = props => {
  const [formStatus, setFormStatus] = useState(FORM_STATUS_ENTRY);
  useEffect(() => getData(props), []);

  const saveData = (values, handleError) => {
    updateEmployee(
      values,
      response => {
        props.clearEmployeesList();
        setFormStatus(FORM_STATUS_SAVED);
      },
      () => {
        setFormStatus(FORM_STATUS_ERROR);
        handleError();
      }
    );
  };

  const { selectedEmployee, hasErrored, isPending } = props; 

  if (hasErrored) {
    return <SaveError />;
  } else if (isPending) {
    return (
      <>
        <div>Fetching data...</div>
        <Loader active inline="centered" />
      </>
    );
  } else {
    if (isNil(selectedEmployee) || !selectedEmployee) {
      return <div>Data is Empty</div>;
    }

    return (
      <div>
        {formStatus === FORM_STATUS_ERROR && <SaveError />}
        {formStatus === FORM_STATUS_SAVED && <SaveSuccess id={props.id} />}
        {(formStatus === FORM_STATUS_ENTRY ||
          formStatus === FORM_STATUS_ERROR) && (
          <EmployeeForm {...selectedEmployee} handleSave={saveData} />
        )}
      </div>
    );
  }
};

const EmployeeEdit = props => {
  let { id } = useParams();
  return (
    <>
      <Info />
      <Card fluid>
        <Card.Content extra>
          <Header as="h3" textAlign="left">
            Edit Employee:
          </Header>
          <Divider hidden />
          <EmployeeEditCore id={id} {...props} />
        </Card.Content>
      </Card>
    </>
  );
};

const mapStateToProps = state => {
  return {
    selectedEmployee: state.employees.selectedEmployee,
    hasErrored: state.employees.hasErrored,
    isPending: state.employees.isPending
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchData: id => dispatch(getEmployee(id)),
    clearEmployeesList: () => dispatch(resetAllEmployees())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EmployeeEdit);
