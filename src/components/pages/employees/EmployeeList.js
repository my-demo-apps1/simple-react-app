import React, { useEffect } from "react";
import { connect } from "react-redux";
import { useParams, Link } from "react-router-dom";
import { isNil } from "lodash";
import EmployeeTable from "../../employees/EmployeeTable";
import getAllEmployees from "../../../actions/employees/GetAll";
import resetEmployee from "../../../actions/employees/Reset";
import MessageBox from "../../extension/MessageBox";
import { Segment, Item, Header, Loader } from "semantic-ui-react";
import TextIcon from "../../extension/TextIcon";

const Info = () => {
  const message = (
    <ul>
      <li>Semantic UI Table</li>
      <li>Redux - Thunks, Reducers, Store</li>
      <li>React Hooks with useState and useEffect</li>
      <li>AWS API Gateway for GET</li>
      <li>AWS Lambda function to read from DynamoDB</li>
      <li>CRUD Operations: ADD/PUT, UPDATE/POST, DELETE</li>
      <li>
        NOTE: API call is invoked only when data is not available in redux
        store.
      </li>
    </ul>
  );
  return <MessageBox title="Key Components in this Demo" message={message} />;
};

const getData = ({ fetchData, clearEmployee, employees }) => {
  clearEmployee();
  if (
    isNil(employees) ||
    isNil(employees.Items) ||
    employees.Items.length < 1
  ) {
    return fetchData();
  }
  return;
};

const GridDemo = props => {
  useEffect(() => getData(props), []);
  const { id } = useParams();
  const { employees, hasErrored, isPending } = props;
  let retVal;
  if (hasErrored) {
    retVal = <div>Error occured while invoking the REST API - (AWS API Gateway + DynamoDB)</div>;
  } else if (isPending) {
    retVal = (
      <>
        <div>Fetching data from  REST API - (AWS API Gateway + DynamoDB) ...</div>
        <Loader active inline="centered" />
      </>
    );
  } else {
    if (isNil(employees) || isNil(employees.Items)) {
      retVal = <div>Loading data</div>;
    } else {
      retVal = (
        <>
          <Segment basic>
            <Header as="h5">List of Employees</Header>

            <Item
              as={Link}
              to={"/dynamictable/employee/add"}
              name="addEmployee"
            >
              <TextIcon hideText={false} color="green" name="add user">
                Add Employee
              </TextIcon>
            </Item>
          </Segment>

          <EmployeeTable
            employeeData={employees.Items}
            showActionColumn={true}
            keyColumn="EmployeeId"
            keyValueToHighLight={id}
          />
        </>
      );
    }
  }
  return (
    <>
      <Info /> 
      <Header as="h3">Dynamic Table Demo</Header>
      {retVal}
    </>
  );
};

const mapStateToProps = state => {
  return {
    employees: state.employees.allEmployees,
    hasErrored: state.employees.hasErrored,
    isPending: state.employees.isPending
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchData: url => dispatch(getAllEmployees()),
    clearEmployee: () => dispatch(resetEmployee())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(GridDemo);
