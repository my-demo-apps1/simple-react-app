import React from "react";
import { Link } from "react-router-dom";
import { Icon, Divider } from "semantic-ui-react";
import { Grid } from "semantic-ui-react";
import MessageBox from "../extension/MessageBox";

const Info = () => {
  const message = (
    <ul>
      <li>Routing with React Router and Redux Connected Router</li>
      <li>Responsive Layout</li>
    </ul>
  );
  return <MessageBox title="Key Components in this Demo" message={message} />;
};

const Home = props => {
  return (
    <Grid>
      <Grid.Row>
        <Grid.Column width={16}>
          <Info />
        </Grid.Column>
        <Grid.Column width={16}>
          <h1 className="display-5">Welcome to my demo app!</h1>
        </Grid.Column>
        <Grid.Column width={16}>
          <p>
            This Single Page Appllication demonstrates most common components
            that will be included in real world enterprise apps. This SPA is
            developed using
          </p>
          <ul>
            <li>ReactJs</li>
            <li>Redux</li>
            <li>React Router / Connected Router</li>
            <li>Formik / Yup</li>
            <li>React Functional Components</li>
            <li>React Hooks</li>
          </ul>
        </Grid.Column>
        <Grid.Column width={16}>
          <p>Common components incude</p>
          <ul>
            <li>Grid / Table</li>
            <li>Tiles</li>
            <li>Entry Forms</li>
            <li>SPA Routing</li>
          </ul>
          <Divider />
        </Grid.Column>
      </Grid.Row>
      <Grid.Row columns={3}>
        <Grid.Column computer={4} mobile={15}>
          <h2>
            <Icon name="th list" /> Dynamic Table
          </h2>
          <p>
            Donec id elit non mi porta gravida at eget metus. Fusce dapibus,
            tellus ac cursus commodo, tortor mauris condimentum nibh, ut
            fermentum massa justo sit amet risus. Etiam porta sem malesuada
            magna mollis euismod. Donec sed odio dui.{" "}
          </p>
          <p>
            <Link className="btn btn-secondary" to="/dynamictable" role="button">
              View Demo &raquo;
            </Link>
          </p>
          <Divider hidden />
        </Grid.Column>
        <Grid.Column computer={4} mobile={15}>
          <h2>
            <Icon name="edit" />
            Forms
          </h2>
          <p>
            Donec sed odio dui. Cras justo odio, dapibus ac facilisis in,
            egestas eget quam. Vestibulum id ligula porta felis euismod semper.
            Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum
            nibh, ut fermentum massa justo sit amet risus.
          </p>
          <p>
            <Link className="btn btn-secondary" to="/forms" role="button">
              View Demo &raquo;
            </Link>
          </p>
          <Divider hidden />
        </Grid.Column>
        <Grid.Column computer={4} mobile={15}>
          <h2>
            <Icon name="boxes" /> Binary Tree Algorithms
          </h2>
          <p>
            Donec id elit non mi porta gravida at eget metus. Fusce dapibus,
            tellus ac cursus commodo, tortor mauris condimentum nibh, ut
            fermentum massa justo sit amet risus. Etiam porta sem malesuada
            magna mollis euismod. Donec sed odio dui.{" "}
          </p>
          <p>
            <Link className="btn btn-secondary" to="/binarytree" role="button">
              View Demo &raquo;
            </Link>
          </p>
          <Divider hidden />
        </Grid.Column>
        <Grid.Column computer={4} mobile={15}>
          <h2>
            <Icon name="chess board" /> Array Algorithms
          </h2>
          <p>
            Donec id elit non mi porta gravida at eget metus. Fusce dapibus,
            tellus ac cursus commodo, tortor mauris condimentum nibh, ut
            fermentum massa justo sit amet risus. Etiam porta sem malesuada
            magna mollis euismod. Donec sed odio dui.{" "}
          </p>
          <p>
            <Link className="btn btn-secondary" to="/arrays" role="button">
              View Demo &raquo;
            </Link>
          </p>
          <Divider hidden />
        </Grid.Column>
        
      </Grid.Row>
    </Grid>
  );
};

export default Home;
