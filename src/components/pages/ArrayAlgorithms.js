import React, { useState } from "react";
import { isNil } from "lodash";
import {
  Label,
  Loader,
  Divider,
  Segment,
  Header,
  Grid
} from "semantic-ui-react";
import NumbersArrayInputAPI from "../forms/NumbersArrayInputAPI";
import getBalancedArray from "../../actions/arrayAPI/BalanceArray";
import doBinarySearch from "../../actions/arrayAPI/BinarySearch";
import MessageBox from "../extension/MessageBox";
import Array from "../extension/Array";

const Info = () => {
  const message = (
    <ul>
      <li>.NET CORE WebAPI is used to process the Arrays</li>
      <li>.NET CORE WebAPI is hosted on AWS Serverless</li>
    </ul>
  );
  return <MessageBox title="Key Components in this Demo" message={message} />;
};

const PrintBAResult = (result, numbersArray) => {
  if (!isNil(result) && result.length > 0) {
    let newlyAddedIndex = -1;
    if (!numbersArray.includes(result[0].toString())) {
      newlyAddedIndex = 0;
    } else if (!numbersArray.includes(result[result.length - 1].toString())) {
      newlyAddedIndex = result.length - 1;
    }

    return (
      <>
        <Label pointing="right">Balanced Array</Label>
        <Array arrayToDisplay={result} indexToHighlight={newlyAddedIndex} />
      </>
    );
  }
  return null;
};
const PrintBSResult = ({ matchingIndex, searchLog }, numbersArray) => {
  const sl =
    !searchLog || searchLog.map(logEntry => <Segment>{logEntry}</Segment>);
  return (
    <>
      <Label pointing="right">Result of Binary Search (matching index)</Label>
      <Label basic as="a" color="red">
        {" "}
        {matchingIndex}{" "}
      </Label>
      <br />
      <br />
      <Array arrayToDisplay={numbersArray} indexToHighlight={matchingIndex} />
      <Divider hidden></Divider>
      <Header as="h4">Search Log:</Header>
      <Segment.Group>{sl}</Segment.Group>
    </>
  );
};
export default props => {
  const [numbers, setNumbers] = useState("8,10,12,5,3,6,1,20,32,7,11,34");
  const [numberToSearch, setNumberToSearch] = useState("3");
  const [action, setAction] = useState("ba");
  const [isLoading, setIsLoading] = useState(false);
  const [hasErrored, setHasErrored] = useState(false);
  const [resultBS, setResultBS] = useState({});
  const [resultBA, setResultBA] = useState([]);

  const handleUpdate = (nums, numberToSearch, action) => {
    setHasErrored(false);
    setIsLoading(true);
    setNumbers(nums);
    setNumberToSearch(numberToSearch);
    setAction(action);
    if (action === "ba") {
      getBalancedArray(
        numbers,
        result => {
          setIsLoading(false);
          setResultBA(result);
        },
        () => {
          setIsLoading(false);
          setHasErrored(true);
        }
      );
    }
    if (action === "bs") {
      doBinarySearch(
        numbers,
        numberToSearch,
        result => {
          setIsLoading(false);
          setResultBS(result);
        },
        () => {
          setIsLoading(false);
          setHasErrored(true);
        }
      );
    }
  };

  const numbersArray = numbers.split(",");

  if (action === "bs") {
    numbersArray.sort(function(a, b) {
      return a - b;
    });
  }

  return (
    <>
      <Info />
      <Header as="h3">Demo of .NET WebAPI + C# Datastructure Algorithms</Header>
      <Grid>
        <Grid.Row columns={2}>
          <Grid.Column computer={6} tablet={15} mobile={15}>
            <NumbersArrayInputAPI
              handleUpdate={handleUpdate}
              numbers={numbers}
              numberToSearch={numberToSearch}
              showNumberToSearch={action === "bs"}
            /> 
          </Grid.Column>
          <Grid.Column computer={8} tablet={15} mobile={15}>
          <Label pointing="right">
              {action === "bs" ? "Soterd Input Array" : "Input Array"}
            </Label>
            {numbersArray.map(number => (
              <Label as="a" color="teal">
                {" "}
                {number}{" "}
              </Label>
            ))}
            <Divider hidden/>
            {isLoading && (
              <>
                <div>Invoking .NET CORE WebAPI to process the array...</div>
                <Loader active inline="centered" />
              </>
            )}
            {hasErrored && (
              <div>Error occured while processing your request</div>
            )}
            {!isLoading &&
              !hasErrored &&
              action === "ba" &&
              PrintBAResult(resultBA, numbersArray)}
            {!isLoading &&
              !hasErrored &&
              action === "bs" &&
              PrintBSResult(resultBS, numbersArray)}
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </>
  );
};
