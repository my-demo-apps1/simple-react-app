import React, { useState } from "react";
import { Message } from "semantic-ui-react";

const MessageBox = ({ title, message, color, size }) => {
  const [showMessage, setShowMessage] = useState(true);

  const handleDismiss = () => {
    setShowMessage(false);
  };

  if (showMessage) {
    return (
      <Message
        color={color || "teal"}
        size={size || "small"}
        onDismiss={handleDismiss}
      >
        <Message.Header>{title}</Message.Header>
        <div>{message}</div>
      </Message>
    );
  }
  return null;
};

export default MessageBox;
