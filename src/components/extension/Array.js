
import React from "react";
import { Label } from "semantic-ui-react";

export default ({arrayToDisplay, indexToHighlight = -1}) => { 
   return arrayToDisplay.map((number, i) => (
    <Label as="a" color={i !== indexToHighlight ? "teal" : "red"}>
      {" "}
      {number}{" "}
    </Label>
  ));
};