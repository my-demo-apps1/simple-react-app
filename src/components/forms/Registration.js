import React from "react";
import { Formik } from "formik";
import { Button, Form } from "semantic-ui-react";
import * as Yup from "yup";
import { Input } from "../controls/index";

const handleSubmit = (values, formikApi) => {
  formikApi.setSubmitting(false);
  // setTimeout(() => {
  //   Object.keys(values).forEach(key => {
  //     formikApi.setFieldError(key, "Some Error");
  //   });
  //   formikApi.setSubmitting(false);
  // }, 1000);
};

/*  onSubmit={fields => {
    alert("SUCCESS!! :-)\n\n" + JSON.stringify(fields, null, 4));
}}*/

export default () => (
  <Formik
    initialValues={{
      firstName: "",
      lastName: "",
      email: "",
      password: "",
      confirmPassword: ""
    }}
    validationSchema={Yup.object().shape({
      firstName: Yup.string().required("First Name is required"),
      lastName: Yup.string().required("Last Name is required"),
      email: Yup.string()
        .email("Email is invalid")
        .required("Email is required"),
      password: Yup.string()
        .min(6, "Password must be at least 6 characters")
        .required("Password is required"),
      confirmPassword: Yup.string()
        .oneOf([Yup.ref("password"), null], "Passwords must match")
        .required("Confirm Password is required")
    })}
    onSubmit={handleSubmit}
  >
    {({ handleSubmit, handleReset, isSubmitting }) => (
      <Form onSubmit={handleSubmit}>
        <Input label="First Name" name="firstName" />
        <Input label="Last Name" name="lastName" />
        <Input label="Email" name="email" />
        <Input
          label="Password"
          name="password"
          inputProps={{
            type: "password"
          }}
        />
        <Input
          label="Confirm Password"
          name="confirmPassword"
          inputProps={{
            type: "password"
          }}
        />
        <Button type="submit" loading={isSubmitting} primary>
          Submit
        </Button>
        <Button type="button" basic onClick={handleReset}>
          Cancel
        </Button>
      </Form>
    )}
  </Formik>
);
