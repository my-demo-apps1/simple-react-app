import React from "react";
import { withFormik } from "formik";
import { Button, Form, Divider } from "semantic-ui-react";
import { Input } from "../controls/index";

const RenderForm = ({ values, handleSubmit}) => (
  <Form onSubmit={handleSubmit}>
    <Input label="Enter integer values separated by coma:" name="numbers" />
    <Button 
    name="bt" 
    type="submit" 
    primary={!values.treeType || values.treeType === "bt"}
    onClick={(e) => {
      values.treeType = "bt";
      handleSubmit();
   }} 
   title="Click to see simple binary tree"
   >
      Binary Tree
    </Button>
    <Button 
    name="bbt"
    type="submit"
    primary={values.treeType && values.treeType === "bbt"}
    onClick={(e) => {
      values.treeType = "bbt";
      handleSubmit();
   }}
    title="Click to see balanced binary tree. The heights of the two child subtrees of any node differ by at most one">
      Balanced Binary Tree
    </Button>
    <Divider hidden />
  </Form>
);

const NumbersArrayInput = withFormik({
  mapPropsToValues: ({ numbers }) => ({
    numbers: numbers
  }),

  // Custom sync validation
  validate: values => {
    const errors = {};

    if (!values.numbers) {
      errors.numbers = "* Required";
    }

    return errors;
  },

  handleSubmit: (values, formikApi) => {
    const {
      props: { handleUpdate }
    } = formikApi;
    const { numbers, treeType} = values; 
    handleUpdate(numbers, treeType);
  },



  displayName: "Binary Search Tree input:"
})(RenderForm);

export default NumbersArrayInput;
