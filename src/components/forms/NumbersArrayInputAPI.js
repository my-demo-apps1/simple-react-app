import React from "react";
import { withFormik } from "formik";
import { Button, Form, Divider } from "semantic-ui-react";
import { Input } from "../controls/index";

const RenderForm = ({ values, handleSubmit, showNumberToSearch }) => (
  <Form onSubmit={handleSubmit}>
    <Input label="Enter integer values separated by coma:" name="numbers" />
    {showNumberToSearch && (
      <Input label="Enter number to search" name="numberToSearch" />
    )}
    <Button
      name="bt"
      type="submit"
      primary={!values.action || values.action === "ba"}
      onClick={e => {
        values.action = "ba";
        handleSubmit();
      }}
      title="Click to see Balanced Array"
    >
      Balance Array
    </Button>
    <Button
      name="bs"
      type="submit"
      primary={values.action && values.action === "bs"}
      onClick={e => {
        values.action = "bs";
        handleSubmit();
      }}
      title="Click to Search array"
    >
      Binary Search
    </Button>
    <Divider hidden />
  </Form>
);

const NumbersArrayInputAPI = withFormik({
  mapPropsToValues: ({ numbers, numberToSearch }) => ({
    numbers: numbers,
    numberToSearch: numberToSearch
  }),

  validate: values => {
    const errors = {};

    if (!values.numbers) {
      errors.numbers = "* Required";
    }

    if (values.action === "bs" && !values.numberToSearch) {
      errors.numberToSearch = "* Required";
    }
    return errors;
  },

  handleSubmit: (values, formikApi) => {
    const {
      props: { handleUpdate }
    } = formikApi;
    const { numbers, numberToSearch, action } = values;
    handleUpdate(numbers, numberToSearch, action);
  },

  displayName: "Array algorithms inputs:"
})(RenderForm);

export default NumbersArrayInputAPI;
