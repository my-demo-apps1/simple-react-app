import React from "react";
import Tree from "../extension/Tree";
import Array from "../extension/Array";
import Node, { getInOrderRec, getPreOrderRec, getPostOrderRec } from "./Node";
import { Divider, Label } from "semantic-ui-react";

class BinarySearchTree {
  constructor() {
    this.root = null;
  }

  // returns root of the tree
  getRootNode = () => {
    return this.root;
  };

  insert = data => {
    const newNode = new Node(data);
    if (!this.root) {
      this.root = newNode;
    } else {
      this.insertNode(this.root, newNode);
    }
  };
  // T-8,R1-10, R2-12,L1-5, L2-3,R2-6,L3-1,R3-20,R4-32,L3-7,11
  insertNode = (node, newNode) => {
    if (node) {
      let isInserted = false;
      if (!node.left && newNode.data < node.data) {
        node.left = newNode;
        isInserted = true;
      }

      if (!node.right && newNode.data >= node.data) {
        node.right = newNode;
        isInserted = true;
      }
      if (!isInserted) {
        if (node.left && newNode.data < node.data) {
          this.insertNode(node.left, newNode);
        }
        if (node.right && newNode.data >= node.data) {
          this.insertNode(node.right, newNode);
        }
      }
    } else {
      node = newNode;
    }
  };

  getInOrder = () => {
    return getInOrderRec(this.root);
  };

  getPreOrder = () => {
    return getPreOrderRec(this.root);
  };

  getPostOrder = () => {
    return getPostOrderRec(this.root);
  };

  remove = data => {
    this.root = this.removeNode(this.root, data);
  };

  removeNode = (node, key) => {
    if (node === null) {
      return null;
    } else if (key < node.data) {
      node.left = this.removeNode(node.left, key);
      return node;
    } else if (key > node.data) {
      node.right = this.removeNode(node.right, key);
      return node;
    } else {
      if (node.left === null && node.right === null) {
        node = null;
        return node;
      }

      if (node.left === null) {
        node = node.right;
        return node;
      } else if (node.right === null) {
        node = node.left;
        return node;
      }

      var aux = this.findMinNode(node.right);
      node.data = aux.data;

      node.right = this.removeNode(node.right, aux.data);
      return node;
    }
  };

  getBalancedBinaryTree = () => {
    const inOrderNumbers = this.getInOrder();
    return this.createBalancedBinaryTree(inOrderNumbers, 0, inOrderNumbers.length - 1);
  };

  createBalancedBinaryTree = (inOrderNumbers, start, end) => {
    if (start > end) {
      return null;
    }
    const mid = Math.round(start + (end - start) / 2);
    const newNodeData = inOrderNumbers[mid];
    if (newNodeData) {
      const newNode = new Node(newNodeData);
      const dataLeft = this.createBalancedBinaryTree(
        inOrderNumbers,
        start,
        mid - 1
      );
      if (dataLeft) {
        newNode.left = dataLeft;
      }
      const dataRight = this.createBalancedBinaryTree(
        inOrderNumbers,
        mid + 1,
        end
      );
      if (dataRight) {
        newNode.right = dataRight;
      }

      return newNode;
    }
    return null;
  };
}

// 7,4,1,10,6,8,3,5
const BinaryTree = ({ numbers = [], balanceTree=false, width, height }) => {
  let bst = new BinarySearchTree();
  numbers.forEach(num => bst.insert(num));
  const rootNode = balanceTree? bst.getBalancedBinaryTree() : bst.getRootNode();
  const inOrder = getInOrderRec(rootNode);
  const preOrder = getPreOrderRec(rootNode);
  const postOrder = getPostOrderRec(rootNode);
  return (
    <>
      <Tree binaryTree={rootNode} width={width} height={height} />
      <Divider hidden />
      <Label pointing="right">In order Traversal&nbsp;&nbsp;</Label>
      <Array arrayToDisplay={inOrder} />
      <br />
      <br />
      <Label pointing="right">Pre order Traversal&nbsp;</Label>
      <Array arrayToDisplay={preOrder} />
      <br />
      <br />
      <Label pointing="right">Post order Traversal</Label>
      <Array arrayToDisplay={postOrder} />
    </>
  );
};

export default BinaryTree;
