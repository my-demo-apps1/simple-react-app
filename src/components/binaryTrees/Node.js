class Node {
  constructor(data) {
    this.data = parseInt(data);
    this.left = null;
    this.right = null;
  }
}

const getInOrderRec = node => {
  let inOrder = [];
  if (node) {
    if (node.left) {
      inOrder = inOrder.concat(getInOrderRec(node.left));
    }
    inOrder.push(node.data);
    if (node.right) {
      inOrder = inOrder.concat(getInOrderRec(node.right));
    }
  }
  return inOrder;
};

const getPreOrderRec = node => {
  let preOrder = [];
  if (node) {
    preOrder.push(node.data);

    if (node.left) {
      preOrder = preOrder.concat(getPreOrderRec(node.left));
    }

    if (node.right) {
      preOrder = preOrder.concat(getPreOrderRec(node.right));
    }
  }
  return preOrder;
};

const getPostOrderRec = node => {
  let postOrder = [];
  if (node) {
    if (node.left) {
      postOrder = postOrder.concat(getPostOrderRec(node.left));
    }

    if (node.right) {
      postOrder = postOrder.concat(getPostOrderRec(node.right));
    }
    postOrder.push(node.data);
  }
  return postOrder;
};

// finds the minimum node in tree
// searching starts from given node
const findMinNode = node => {
  if (node.left === null) {
    return node;
  } else {
    return findMinNode(node.left);
  }
};

const search = (node, data) => {
  if (node === null) {
    return null;
  } else if (data < node.data) {
    return search(node.left, data);
  } else if (data > node.data) {
    return search(node.right, data);
  } else {
    return node;
  }
};

export { findMinNode, getInOrderRec, getPostOrderRec, getPreOrderRec, search };
export default Node;
