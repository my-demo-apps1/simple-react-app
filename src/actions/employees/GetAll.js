import {
  GET_ALL_EMPLOYEES_SUCCESS,
  GET_ALL_EMPLOYEES_PENDING,
  GET_ALL_EMPLOYEES_ERROR
} from "../../reducers/Employees";

const getAllErrored = bool => {
  return {
    type: GET_ALL_EMPLOYEES_ERROR,
    hasErrored: bool
  };
};

const gaetAllIsPending = bool => {
  return {
    type: GET_ALL_EMPLOYEES_PENDING,
    isPending: bool
  };
};

const getAllSuccess = employees => {
  return {
    type: GET_ALL_EMPLOYEES_SUCCESS,
    employees
  };
};

const getAllEmployees = () => {
  const url = process.env.REACT_APP_API;
  const apiToken = process.env.REACT_APP_API_TOKEN
  const options = {
    method: "GET",
    headers: {
      //'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token'
      "x-api-key": apiToken,
      "content-type": "application/json"
    },
    // mode: 'no-cors'
  };
  return dispatch => {
    dispatch(gaetAllIsPending(true));
    fetch(url, options)
      .then(response => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(gaetAllIsPending(false));
        return response;
      })
      .then(response => response.json())
      .then(items => dispatch(getAllSuccess(items)))
      .catch(() => dispatch(getAllErrored(true)));
  };
};

export default getAllEmployees;
