const addEmployee = (
  { firstname, lastname, title, department},
  handleSucess,
  handleError
) => {
  const url = process.env.REACT_APP_API;
  const apiToken = process.env.REACT_APP_API_TOKEN;
  const params =  { 
    Firstname: firstname,
    Lastname: lastname,
    Title: title,
    Department: department
  };
  const options = {
    method: "PUT",
    headers: {
      //'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token'
      "x-api-key": apiToken,
      "content-type": "application/json"
    },
    body: JSON.stringify(params)
    // mode: 'no-cors'
  };
  return fetch(url, options)
    .then(response => {
      if (!response.ok) {
        throw Error(response.statusText);
      }
      return response;
    })
    .then(response => handleSucess(response.json()))
    .catch(() => handleError());
};

export default addEmployee;
