import {
    GET_EMPLOYEE_SUCCESS,
    GET_EMPLOYEE_PENDING,
    GET_EMPLOYEE_ERROR
  } from "../../reducers/Employees";
  
  const getErrored = bool => {
    return {
      type: GET_EMPLOYEE_ERROR,
      hasErrored: bool
    };
  };
  
  const getIsPending = bool => {
    return {
      type: GET_EMPLOYEE_PENDING,
      isPending: bool
    };
  };
  
  const getSuccess = employee => {
    return {
      type: GET_EMPLOYEE_SUCCESS,
      employee
    };
  };
  
  const getEmployee = (id) => {
    const url = process.env.REACT_APP_API + "?id=" + id;
    const apiToken = process.env.REACT_APP_API_TOKEN
    const options = {
      method: "GET",
      headers: {
        "x-api-key": apiToken
      }
      // mode: 'no-cors'
    };
    return dispatch => {
      dispatch(getIsPending(true));
      fetch(url, options)
        .then(response => {
          if (!response.ok) {
            throw Error(response.statusText);
          }
          dispatch(getIsPending(false));
          return response;
        })
        .then(response => response.json())
        .then(items => dispatch(getSuccess(items)))
        .catch(() => dispatch(getErrored(true)));
    };
  };
  
  export default getEmployee;
  