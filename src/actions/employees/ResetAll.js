import { RESET_ALL_EMPLOYEES } from "../../reducers/Employees";

const reset = () => {
return {
    type: RESET_ALL_EMPLOYEES,
    employees: null
  };
}
const resetAllEmployees = () => {
  return dispatch => {
    dispatch(reset());
  };
};

export default resetAllEmployees;
