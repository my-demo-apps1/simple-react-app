import { RESET_EMPLOYEE } from "../../reducers/Employees";

const reset = () => {
return {
    type: RESET_EMPLOYEE,
    employee: null
  };
}
const resetEmployee = () => {
  return dispatch => {
    dispatch(reset());
  };
};

export default resetEmployee;
