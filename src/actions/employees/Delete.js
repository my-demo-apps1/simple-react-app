const deleteEmployee = (
  id,
  handleSucess,
  handleError
) => {
  const url = process.env.REACT_APP_API + "?id=" + id;
  const apiToken = process.env.REACT_APP_API_TOKEN;
  const options = {
    method: "DELETE",
    headers: {
      //'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token'
      "x-api-key": apiToken,
      "content-type": "application/json"
    },
    // mode: 'no-cors'
  };
  return fetch(url, options)
    .then(response => {
      if (!response.ok) {
        throw Error(response.statusText);
      }
      return response;
    })
    .then(response => handleSucess(response))
    .catch(() => handleError());
};

export default deleteEmployee;
