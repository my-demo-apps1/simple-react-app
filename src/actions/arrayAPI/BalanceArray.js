const getBalancedArray = (
  numbersArray,
  handleSucess,
  handleError
) => {
  const url = process.env.REACT_APP_WEBAPI + 'balancearray/';
  const params =  {
    NumbersArray: numbersArray.split(",")
  };
  const options = {
    method: "POST",
    headers: {
      "content-type": "application/json"
    },
    body: JSON.stringify(params),
    // mode: 'no-cors'
  };
  return fetch(url, options)
    .then(response => {
      if (!response.ok) {
        throw Error(response.statusText);
      }
      return response.json();
    })
    .then(data =>{
       handleSucess(data)
      })
    .catch(() => handleError());
};

export default getBalancedArray;
