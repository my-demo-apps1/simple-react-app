const doBinarysearch = (
  numbersArray,
  numberToSearch,
  handleSucess,
  handleError
) => {
  const url = process.env.REACT_APP_WEBAPI + "binarysearch/";
  const numbersArraySorted = numbersArray.split(",");

  numbersArraySorted.sort(function(a, b) {
    return a - b;
  });

  const params = {
    NumbersArray: numbersArraySorted,
    NumberToSearch: numberToSearch
  };
  const options = {
    method: "POST",
    headers: {
      "content-type": "application/json"
    },
    body: JSON.stringify(params)
    // mode: 'no-cors'
  };
  return fetch(url, options)
    .then(response => {
      if (!response.ok) {
        throw Error(response.statusText);
      }
      return response.json();
    })
    .then(data => {
      handleSucess(data);
    })
    .catch(() => handleError());
};

export default doBinarysearch;
