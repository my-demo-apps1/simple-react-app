export const GET_ALL_EMPLOYEES_SUCCESS = "GET_ALL_EMPLOYEES_SUCCESS";
export const GET_ALL_EMPLOYEES_PENDING = "GET_ALL_EMPLOYEES_PENDING";
export const GET_ALL_EMPLOYEES_ERROR = "GET_ALL_EMPLOYEES_ERROR";
export const GET_EMPLOYEE_SUCCESS = "GET_EMPLOYEE_SUCCESS";
export const GET_EMPLOYEE_PENDING = "GET_EMPLOYEE_PENDING";
export const GET_EMPLOYEE_ERROR = "GET_EMPLOYEE_ERROR";
export const RESET_EMPLOYEE = "RESET_EMPLOYEE";
export const RESET_ALL_EMPLOYEES = "RESET_ALL_EMPLOYEES";

// const PUT_EMPLOYEE = "PUT_EMPLOYEE";
// const UPDATE_EMPLOYEE = "UPDATE_EMPLOYEE";
// const DELETE_EMPLOYEE = "DELETE_EMPLOYEE";

const initialState = {
  allEmployees: [],
  selectedEmployee: null,
  isPending: false,
  hasErrored: false
};

const employeesReducer = (state, action) => {
  state = state || initialState;
  switch (action.type) {
    case GET_ALL_EMPLOYEES_SUCCESS:
      return { ...state, allEmployees: action.employees };
    case GET_ALL_EMPLOYEES_PENDING:
      return { ...state, isPending: action.isPending };
    case GET_ALL_EMPLOYEES_ERROR:
      return { ...state, hasErrored: action.hasErrored };
    case RESET_ALL_EMPLOYEES:
      return { ...state, allEmployees: [],  isPending: false, hasErrored: false };
    case GET_EMPLOYEE_SUCCESS:
      return { ...state, selectedEmployee: action.employee };
    case GET_EMPLOYEE_PENDING:
      return { ...state, isPending: action.isPending };
    case GET_EMPLOYEE_ERROR:
      return { ...state, hasErrored: action.hasErrored };
    case RESET_EMPLOYEE:
      return { ...state, selectedEmployee: null, isPending: false, hasErrored: false };
    default:
      return state;
  }
};

export default employeesReducer;
