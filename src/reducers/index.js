import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router';
import employeesReducer from './Employees';
import sideMenuReducer from './SideMenu';



const createRootReducer = (history) => combineReducers({
  router: connectRouter(history),
  employees: employeesReducer, 
  sideMenu: sideMenuReducer
});


export default createRootReducer;