﻿const TOGGLE_MENU = 'TOGGLE_MENU';
const initialState = { smallMenu: false };

export const actionCreators = {
  toggleSideMenu: () => ({ type: TOGGLE_MENU }),
};

const sideMenuReducer = (state, action) => {
  state = state || initialState;

  if (action.type === TOGGLE_MENU) {
    return { ...state, smallMenu: !state.smallMenu};
  }

  return state;
};

export default sideMenuReducer;
