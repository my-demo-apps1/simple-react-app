import React from "react";
import { Provider } from 'react-redux';
import AppLayout from "./components/layout";
import configureStore, { history } from "./store";
import { ConnectedRouter } from 'connected-react-router'
import "./App.css";
import "semantic-ui-css/semantic.min.css";

const store = configureStore({})

function App() {
  return (
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <AppLayout />
      </ConnectedRouter>
    </Provider>
  );
}

export default App;
